package httpd

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/env"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/routes"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"

	"github.com/levicook/slog"
	"github.com/robmerrell/comandante"
)

const cmdName = "httpd"
const cmdHelp = ""

func Command() *comandante.Command {
	return comandante.NewCommand(cmdName, cmdHelp, func() error {
		slog.SetPrefix(cmdName)

		repos.OpenDefaultSession()
		defer repos.CloseDefaultSession()

		bind := ":" + env.PORT()
		slog.Printf("listening on %q", bind)

		router := routes.Routes.Router()
		return http.ListenAndServe(bind, router)
	})
}
