package send

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/status"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"

	"github.com/levicook/slog"
)

type errDoc struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}

func Json(w http.ResponseWriter, status int, v interface{}) {
	// note: order matters here:
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	slog.PanicIf(json.NewEncoder(w).Encode(v))
}

func BadRequest(w http.ResponseWriter) {
	Json(w, status.BadRequest, errDoc{
		Code: status.BadRequest,
		Text: "Bad Request",
	})
}

func Forbidden(w http.ResponseWriter) {
	http.Error(w, "", status.Forbidden)
}

func InternalServerError(w http.ResponseWriter) {
	http.Error(w, "", status.InternalServerError)
}

func LengthRequired(w http.ResponseWriter) {
	http.Error(w, "", status.LengthRequired)
}

func NotAcceptable(w http.ResponseWriter) {
	Json(w, status.NotAcceptable, errDoc{
		Code: status.NotAcceptable,
		Text: "Not Acceptable",
	})
}

func NotFound(w http.ResponseWriter) {
	Json(w, status.NotFound, errDoc{
		Code: status.NotFound,
		Text: "Not Found",
	})
}

func NotModified(w http.ResponseWriter) {
	http.Error(w, "", status.NotModified)
}

func RequestEntityTooLarge(w http.ResponseWriter) {
	http.Error(w, "", status.RequestEntityTooLarge)
}

func Unauthorized(w http.ResponseWriter) {
	Json(w, status.Unauthorized, errDoc{
		Code: status.Unauthorized,
		Text: "Not Unauthorized",
	})
}

func UnprocessableEntity(w http.ResponseWriter, err error) {
	switch err.(type) {
	case models.CompositeErrors, models.Errors:
		Json(w, status.UnprocessableEntity, err)
	default:
		Json(w, status.UnprocessableEntity, errDoc{
			Code: status.UnprocessableEntity,
			Text: err.Error(),
		})
	}
}

func UnsupportedMediaType(w http.ResponseWriter) {
	http.Error(w, "", status.UnsupportedMediaType)
}
