package authentication

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
)

func Handler(inner http.Handler, requiresAuth bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if requiresAuth && auth.UserIdFor(r).Blank() {
			send.Unauthorized(w)
			return
		}

		inner.ServeHTTP(w, r)
	})
}
