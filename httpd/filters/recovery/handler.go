package recovery

import (
	"net/http"
	"time"

	"github.com/levicook/slog"
)

func Handler(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		if err := recover(); err != nil {
			slog.Printf(
				"%s %s | rid:%s | %s | %s | error: %v",
				r.Method,
				r.RequestURI,
				r.Header.Get("Request-Id"),
				name,
				time.Since(start),
				err,
			)
		}
	})
}
