package request_id

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

func Handler(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set("Request-Id", bson.NewObjectId().Hex())
		w.Header().Set("Request-Id", r.Header.Get("Request-Id"))

		inner.ServeHTTP(w, r)
	})
}
