package endpoints

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/status"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos/fakes"
)

func Test_ReviewRequestAnswerOptionList_NotAcceptable(t *testing.T) {
	repos.OpenDefaultSession()
	defer repos.CloseDefaultSession()

	w := httptest.NewRecorder()
	r := newJsonRequest(nil)

	ReviewRequestAnswerOptionList(w, r)

	h := w.Header()
	assert.Equal(t, "application/json; charset=UTF-8", h.Get("Content-Type"))
	assert.Empty(t, h.Get("ETag"))

	assert.Equal(t, status.NotAcceptable, w.Code)
}

func Test_reviewRequestAnswerOptionList_BadRequest(t *testing.T) {
	w := httptest.NewRecorder()
	r := newJsonRequest(nil)
	v := router.Vars{}
	i := auth.CanDoNothing

	r.Header.Set("Accept", V1)
	reviewRequestAnswerOptionList(w, r, v, i, nil)

	h := w.Header()
	assert.Equal(t, "application/json; charset=UTF-8", h.Get("Content-Type"))
	assert.Empty(t, h.Get("ETag"))

	assert.Equal(t, status.BadRequest, w.Code)
}

func Test_reviewRequestAnswerOptionList_Forbidden(t *testing.T) {
	w := httptest.NewRecorder()
	r := newJsonRequest(nil)
	v := router.Vars{"reviewRequestId": "5490d81f87150da7d4000001"}
	i := auth.CanDoNothing
	repo := fakes.FakeAnswerOptionRepo{
		FakeAllByReviewRequestId: func(models.ReviewRequestId) (c models.AnswerOptions) { return },
	}

	r.Header.Set("Accept", V1)
	reviewRequestAnswerOptionList(w, r, v, i, repo)

	h := w.Header()
	assert.Equal(t, "text/plain; charset=utf-8", h.Get("Content-Type"))
	assert.Empty(t, h.Get("ETag"))

	assert.Equal(t, status.Forbidden, w.Code)
}

func Test_reviewRequestAnswerOptionList_OK(t *testing.T) {
	w := httptest.NewRecorder()
	r := newJsonRequest(nil)
	v := router.Vars{"reviewRequestId": "5490d81f87150da7d4000001"}
	i := auth.CanDoAnything
	repo := fakes.FakeAnswerOptionRepo{
		FakeAllByReviewRequestId: func(models.ReviewRequestId) (c models.AnswerOptions) { return },
	}

	r.Header.Set("Accept", V1)
	reviewRequestAnswerOptionList(w, r, v, i, repo)

	h := w.Header()
	assert.Equal(t, "application/json; charset=UTF-8", h.Get("Content-Type"))
	assert.NotEmpty(t, h.Get("ETag"))

	assert.Equal(t, status.OK, w.Code)
}
