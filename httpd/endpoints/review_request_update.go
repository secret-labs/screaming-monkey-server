package endpoints

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/read"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
	"github.com/gorilla/mux"
)

func ReviewRequestUpdate(w http.ResponseWriter, r *http.Request) {
	reviewRequestUpdate(w, r,
		mux.Vars(r),
		auth.IdentityFor(r),
		repos.NewReviewRequestRepo(),
	)
}

func reviewRequestUpdate(
	w http.ResponseWriter,
	r *http.Request,
	v router.Vars,
	i auth.Identity,
	repo repos.ReviewRequestRepo,
) {
	var o models.ReviewRequest

	if v.ReviewRequestId().Invalid() {
		send.BadRequest(w)
		return
	}

	if e := read.Json(r, &o); e != nil {
		send.UnprocessableEntity(w, e)
		return
	}

	if o.Id != v.ReviewRequestId() || !i.CanUpdate(o) {
		send.Forbidden(w)
		return
	}

	if e := repo.Update(v.ReviewRequestId(), &o); e.Present() {
		send.UnprocessableEntity(w, e)
		return
	}

	sendUpdateJson(w, r, o)
}
