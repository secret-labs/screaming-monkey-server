package endpoints

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/read"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
	"github.com/gorilla/mux"
)

func ReviewRequestCreate(w http.ResponseWriter, r *http.Request) {
	reviewRequestCreate(w, r,
		mux.Vars(r),
		auth.IdentityFor(r),
		repos.NewReviewRequestRepo(),
	)
}

func reviewRequestCreate(
	w http.ResponseWriter,
	r *http.Request,
	v router.Vars,
	i auth.Identity,
	repo repos.ReviewRequestRepo,
) {
	var o models.ReviewRequest

	if e := read.Json(r, &o); e != nil {
		send.UnprocessableEntity(w, e)
		return
	}

	if !i.CanCreate(o) {
		send.Forbidden(w)
		return
	}

	if e := repo.Create(&o); e.Present() {
		send.UnprocessableEntity(w, e)
		return
	}

	sendCreateJson(w, r, o)
}
