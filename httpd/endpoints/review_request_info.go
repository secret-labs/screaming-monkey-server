package endpoints

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
	"github.com/gorilla/mux"
)

func ReviewRequestInfo(w http.ResponseWriter, r *http.Request) {
	reviewRequestInfo(w, r,
		mux.Vars(r),
		auth.IdentityFor(r),
		repos.NewReviewRequestRepo(),
	)
}

func reviewRequestInfo(
	w http.ResponseWriter,
	r *http.Request,
	v router.Vars,
	i auth.Identity,
	repo repos.ReviewRequestRepo,
) {

	if v.ReviewRequestId().Invalid() {
		send.BadRequest(w)
		return
	}

	o := repo.OneById(v.ReviewRequestId())

	if !i.CanInfo(o) {
		send.Forbidden(w)
		return
	}

	sendInfoJson(w, r, o)
}
