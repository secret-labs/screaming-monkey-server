package endpoints

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
	"github.com/gorilla/mux"
)

func ReviewRequestQuestionList(w http.ResponseWriter, r *http.Request) {
	reviewRequestQuestionList(w, r,
		mux.Vars(r),
		auth.IdentityFor(r),
		repos.NewQuestionRepo(),
	)
}

func reviewRequestQuestionList(
	w http.ResponseWriter,
	r *http.Request,
	v router.Vars,
	i auth.Identity,
	repo repos.QuestionRepo,
) {

	if v.ReviewRequestId().Invalid() {
		send.BadRequest(w)
		return
	}

	o := repo.AllByReviewRequestId(v.ReviewRequestId())

	if !i.CanList(o) {
		send.Forbidden(w)
		return
	}

	sendListJson(w, r, o)
}
