package endpoints

import (
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
)

func ReviewRequestAnswerOptionList(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	i := auth.IdentityFor(r)
	repo := repos.NewAnswerOptionRepo()

	reviewRequestAnswerOptionList(w, r, v, i, repo)
}

func reviewRequestAnswerOptionList(
	w http.ResponseWriter,
	r *http.Request,
	v router.Vars,
	i auth.Identity,
	repo repos.AnswerOptionRepo,
) {
	if !acceptableVersion(r, V1) {
		send.NotAcceptable(w)
		return
	}

	if v.ReviewRequestId().Invalid() {
		send.BadRequest(w)
		return
	}

	o := repo.AllByReviewRequestId(v.ReviewRequestId())

	if !i.CanList(o) {
		send.Forbidden(w)
		return
	}

	sendListJson(w, r, o)
}
