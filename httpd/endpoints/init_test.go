package endpoints

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"github.com/levicook/slog"
)

func newJsonRequest(v interface{}) *http.Request {
	r, e := http.NewRequest("", "", newJsonRequestBody(v))
	slog.PanicIf(e)
	return r
}

func newJsonRequestBody(v interface{}) io.Reader {
	raw, err := json.Marshal(v)
	slog.PanicIf(err)
	return bytes.NewBuffer(raw)
}
