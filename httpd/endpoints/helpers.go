package endpoints

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/levicook/slog"

	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/status"
)

const (
	V1 = "application/vnd.screaming-monkey+json; version=1"
	V2 = "application/vnd.screaming-monkey+json; version=2"
)

var (
	contains = strings.Contains
	panicIf  = slog.PanicIf
	sprintf  = fmt.Sprintf
)

type created interface {
	Created() bool
}

func acceptableVersion(r *http.Request, versions ...string) bool {
	accept := r.Header.Get(`Accept`)
	for i := range versions {
		if contains(accept, versions[i]) {
			return true
		}
	}
	return false
}

func etagFor(v interface{}) string {
	data, err := json.Marshal(v)
	panicIf(err)

	hasher := md5.New()
	hasher.Write(data)

	sum := hasher.Sum(nil)

	return sprintf("%x", sum)
}

func notModified(r *http.Request, etag string) bool {
	return r.Header.Get(`If-None-Match`) == etag
}

func sendCreateJson(w http.ResponseWriter, r *http.Request, o interface{}) {
	w.Header().Set("ETag", etagFor(o))
	send.Json(w, status.Created, o)
}

func sendInfoJson(w http.ResponseWriter, r *http.Request, o created) {
	if !o.Created() {
		send.NotFound(w)
		return
	}

	sendTaggedJson(w, r, o)
}

func sendListJson(w http.ResponseWriter, r *http.Request, o interface{}) {
	sendTaggedJson(w, r, o)
}

func sendUpdateJson(w http.ResponseWriter, r *http.Request, o interface{}) {
	w.Header().Set("ETag", etagFor(o))
	send.Json(w, status.OK, o)
}

func sendTaggedJson(w http.ResponseWriter, r *http.Request, o interface{}) {
	t := etagFor(o)

	if notModified(r, t) {
		send.NotModified(w)
		return
	}

	w.Header().Set("ETag", t)
	send.Json(w, status.OK, o)
}
