package endpoints

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/auth"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/send"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
)

func ReviewRequestList(w http.ResponseWriter, r *http.Request) {
	reviewRequestList(w, r,
		auth.IdentityFor(r),
		repos.NewReviewRequestRepo(),
	)
}

func reviewRequestList(
	w http.ResponseWriter,
	r *http.Request,
	i auth.Identity,
	repo repos.ReviewRequestRepo,
) {
	o := repo.AllByUserId(i.UserId())

	if !i.CanList(o) {
		send.Forbidden(w)
		return
	}

	sendListJson(w, r, o)
}
