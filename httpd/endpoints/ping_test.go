package endpoints

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_Ping(t *testing.T) {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "/ping", nil)

	Ping(w, r)

	require.Equal(t, 200, w.Code)
	require.Equal(t, "pong\n", w.Body.String())
}
