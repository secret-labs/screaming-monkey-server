package routes

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/endpoints"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/router"
)

var Routes = router.RouteSet{

	// review request routes:
	// ---------------------------------
	{
		"review_request_info", requiresAuthentication,
		"GET", "/api/review-requests/{reviewRequestId}", endpoints.ReviewRequestInfo,
	}, {
		"review_request_list", requiresAuthentication,
		"GET", "/api/review-requests", endpoints.ReviewRequestList,
	}, {
		"review_request_create", requiresAuthentication,
		"POST", "/api/review-requests", endpoints.ReviewRequestCreate,
	}, {
		"review_request_update", requiresAuthentication,
		"PUT", "/api/review-requests/{reviewRequestId}", endpoints.ReviewRequestUpdate,
	},

	// review request descendant routes:
	// ---------------------------------
	{
		"review_request_answer_option_list", requiresAuthentication,
		"GET", "/api/review-requests/{reviewRequestId}/answer-options", endpoints.ReviewRequestAnswerOptionList,
	}, {
		"review_request_question_list", requiresAuthentication,
		"GET", "/api/review-requests/{reviewRequestId}/questions", endpoints.ReviewRequestQuestionList,
	},

	// review responses routes:
	// ---------------------------------
	{
		"review_response_update", requiresAuthentication,
		"PUT", "/api/review-responses/{reviewRequestId}", endpoints.ReviewResponseUpdate,
	},

	// backend debug routes:
	// ---------------------------------
	{
		"ping", optionalAuthentication,
		"GET", "/ping", endpoints.Ping,
	},
}

var (
	optionalAuthentication = false
	requiresAuthentication = true
)
