package routes

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRoutes(t *testing.T) {
	type vars map[string]string

	router := Routes.Router()

	tests := []struct {
		name, method, path string
		vars               vars
	}{

		{"review_request_create",
			"POST", "/api/review-requests", vars{}},

		{"review_request_info",
			"GET", "/api/review-requests/1234", vars{"reviewRequestId": "1234"}},

		{"review_request_list",
			"GET", "/api/review-requests", vars{}},

		{"review_request_update",
			"PUT", "/api/review-requests/1234", vars{"reviewRequestId": "1234"}},

		{"review_request_answer_option_list",
			"GET", "/api/review-requests/1234/answer-options", vars{"reviewRequestId": "1234"}},

		{"review_request_question_list",
			"GET", "/api/review-requests/1234/questions", vars{"reviewRequestId": "1234"}},

		{"review_response_update",
			"PUT", "/api/review-responses/1234", vars{"reviewRequestId": "1234"}},

		{"ping", "GET", "/ping", vars{}},
	}

	assert.Equal(t, len(tests), len(Routes))

	for _, test := range tests {
		req, err := http.NewRequest(test.method, test.path, nil)
		assert.Nil(t, err)

		var match mux.RouteMatch

		found := router.Match(req, &match)
		require.True(t, found, fmt.Sprintf("route %q was not found", test.name))
		require.Equal(t, test.name, match.Route.GetName())
		require.Equal(t, test.vars, vars(match.Vars))
	}
}

func TestRoutesAreUnique(t *testing.T) {
	signatureCounter := make(map[string]int)

	for _, route := range Routes {
		signature := fmt.Sprintf("%v %v", route.Method, route.Pattern)

		signatureCounter[signature]++

		require.Equal(t, 1,
			signatureCounter[signature],
			fmt.Sprintf("duplicate signature %q", signature),
		)
	}
}
