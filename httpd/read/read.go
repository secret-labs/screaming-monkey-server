package read

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/levicook/slog"
)

func Json(r *http.Request, v interface{}) error {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 10*MB))
	slog.PanicIf(err)
	slog.PanicIf(r.Body.Close())
	return json.Unmarshal(body, v)
}

const (
	_  = iota // ignore first value
	KB = 1 << (10 * iota)
	MB
	GB
)
