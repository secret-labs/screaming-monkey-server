package router

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/filters/authentication"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/filters/cors"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/filters/logging"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/filters/recovery"
	"bitbucket.org/secret-labs/screaming-monkey-server/httpd/filters/request_id"

	"github.com/gorilla/mux"
)

type RouteSet []Route

func (routes RouteSet) Router() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = authentication.Handler(handler, route.RequiresAuth)
		handler = cors.Handler(handler)
		handler = request_id.Handler(handler)
		handler = logging.Handler(handler, route.Name)
		handler = recovery.Handler(handler, route.Name) // make sure recovery is always last

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)

		switch route.Method {
		case "DELETE", "PATCH", "PUT":
			router.
				Methods("POST").
				Headers("X-HTTP-Method-Override", route.Method).
				Path(route.Pattern).
				Handler(handler)
		}
	}

	return router
}
