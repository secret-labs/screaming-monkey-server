package router

import (
	"fmt"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"
)

type Vars map[string]string

func (v Vars) ReviewRequestId() models.ReviewRequestId {
	return models.ReviewRequestId(v["reviewRequestId"])
}

func (v Vars) Uint(key string) (val uint) {
	fmt.Sscanf(v[key], "%d", &val)
	return
}
