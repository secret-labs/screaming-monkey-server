package router

import "net/http"

type Route struct {
	Name         string
	RequiresAuth bool
	Method       string
	Pattern      string
	HandlerFunc  http.HandlerFunc
}
