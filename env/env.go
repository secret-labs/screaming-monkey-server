package env

import (
	"fmt"
	"os"
	"strings"
)

func APP_ENV() string          { return strings.ToLower(find("APP_ENV")) }
func APP_HOME() string         { return find("APP_HOME") }
func AUTH_PRIVATE_KEY() string { return find("AUTH_PRIVATE_KEY") }
func AUTH_PUBLIC_KEY() string  { return find("AUTH_PUBLIC_KEY") }
func MONGO_URI() string        { return find("MONGO_URI", "MONGOLAB_URI", "MONGOHQ_URL") }
func PORT() string             { return find("PORT") }

func find(keys ...string) string {
	for _, key := range keys {
		if v := os.Getenv(key); v != "" {
			return v
		}
	}

	panic(fmt.Errorf("env not found: %q", keys))
}
