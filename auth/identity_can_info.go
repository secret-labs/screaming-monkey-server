package auth

func (rc realIdentity) CanInfo(thing interface{}) bool {
	if rc.authedUser.SuperUser {
		return true
	}

	return false
}
