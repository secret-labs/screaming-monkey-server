package auth

import "bitbucket.org/secret-labs/screaming-monkey-server/models"

var CanDoAnything canDoAnything

type canDoAnything struct{}

func (canDoAnything) UserId() models.UserId            { return "" }
func (canDoAnything) CanCreate(thing interface{}) bool { return true }
func (canDoAnything) CanDelete(thing interface{}) bool { return true }
func (canDoAnything) CanInfo(thing interface{}) bool   { return true }
func (canDoAnything) CanList(thing interface{}) bool   { return true }
func (canDoAnything) CanUpdate(thing interface{}) bool { return true }
