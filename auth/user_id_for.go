package auth

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/env"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"

	jwt "github.com/dgrijalva/jwt-go"
)

func UserIdFor(r *http.Request) (id models.UserId) {
	token, err := jwt.ParseFromRequest(r, func(*jwt.Token) (interface{}, error) {
		return []byte(env.AUTH_PUBLIC_KEY()), nil
	})

	if err == nil && token.Valid {
		id = models.UserId(token.Claims["uid"].(string))
	}

	return
}
