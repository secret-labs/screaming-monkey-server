package auth

func (rc realIdentity) CanDelete(thing interface{}) bool {
	if rc.authedUser.SuperUser {
		return true
	}

	return false
}
