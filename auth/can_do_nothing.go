package auth

import "bitbucket.org/secret-labs/screaming-monkey-server/models"

var CanDoNothing canDoNothing

type canDoNothing struct{}

func (canDoNothing) UserId() models.UserId            { return "" }
func (canDoNothing) CanCreate(thing interface{}) bool { return false }
func (canDoNothing) CanDelete(thing interface{}) bool { return false }
func (canDoNothing) CanInfo(thing interface{}) bool   { return false }
func (canDoNothing) CanList(thing interface{}) bool   { return false }
func (canDoNothing) CanUpdate(thing interface{}) bool { return false }
