package auth

func (rc realIdentity) CanUpdate(thing interface{}) bool {
	if rc.authedUser.SuperUser {
		return true
	}

	return false
}
