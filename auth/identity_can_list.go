package auth

func (rc realIdentity) CanList(thing interface{}) bool {
	if rc.authedUser.SuperUser {
		return true
	}

	return false
}
