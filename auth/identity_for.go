package auth

import (
	"net/http"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
)

func IdentityFor(r *http.Request) Identity {
	return identityFor(
		UserIdFor(r),
		repos.NewUserRepo(),
	)
}

func identityFor(authedUserId models.UserId, userRepo repos.UserRepo) Identity {
	return realIdentity{
		authedUser: userRepo.OneById(authedUserId),
	}
}
