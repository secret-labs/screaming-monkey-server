package auth

func (rc realIdentity) CanCreate(thing interface{}) bool {
	if rc.authedUser.SuperUser {
		return true
	}

	return false
}
