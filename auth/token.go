package auth

import (
	"time"

	"bitbucket.org/secret-labs/screaming-monkey-server/env"
	"bitbucket.org/secret-labs/screaming-monkey-server/models"

	"github.com/dgrijalva/jwt-go"
	"github.com/levicook/slog"
)

type Token string

func NewToken(uid models.UserId) Token {
	token := jwt.New(jwt.GetSigningMethod("RS256"))

	token.Claims["iat"] = time.Now().Unix()
	token.Claims["exp"] = time.Now().Add(60 * time.Minute).Unix()
	token.Claims["uid"] = string(uid)

	signedString, err := token.SignedString([]byte(env.AUTH_PRIVATE_KEY()))
	slog.PanicIf(err)
	return Token(signedString)
}
