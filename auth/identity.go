package auth

import "bitbucket.org/secret-labs/screaming-monkey-server/models"

type Identity interface {
	UserId() models.UserId
	CanCreate(thing interface{}) bool
	CanDelete(thing interface{}) bool
	CanInfo(thing interface{}) bool
	CanList(thing interface{}) bool
	CanUpdate(thing interface{}) bool
}

type realIdentity struct {
	authedUser models.User
}

func (o realIdentity) UserId() models.UserId {
	return o.authedUser.Id
}
