package main

import (
	"fmt"
	"os"

	"bitbucket.org/secret-labs/screaming-monkey-server/httpd"
	"bitbucket.org/secret-labs/screaming-monkey-server/seed"

	"github.com/robmerrell/comandante"
)

func main() {
	app := comandante.New("screaming-monkey-server", "")
	app.IncludeHelp() // comandante can create a "help" command for you

	// register all subcommands
	app.RegisterCommand(seed.Command())
	app.RegisterCommand(httpd.Command())

	if err := app.Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
