package repos

import (
	"sync/atomic"
	"testing"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"github.com/stretchr/testify/require"
	"gopkg.in/mgo.v2"
)

var nextId uint32 = 0

func nextTestDbName() string {
	atomic.AddUint32(&nextId, 1)
	return sprintf("screamin_monkey_test_%v", nextId)
}

func seedQuestion(t *testing.T, db *mgo.Database, o *models.Question) {
	repo := newQuestionRepo(db)
	require.Equal(t, noErrors, repo.Create(o))
}

func seedQuestionnaire(t *testing.T, db *mgo.Database, o *models.Questionnaire) {
	repo := newQuestionnaireRepo(db)
	require.Equal(t, noErrors, repo.Create(o))
}

func seedReviewRequest(t *testing.T, db *mgo.Database, o *models.ReviewRequest) {
	repo := newReviewRequestRepo(db)
	require.Equal(t, noErrors, repo.Create(o))
}
