package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type AnswerOptionRepo interface {
	AllByReviewRequestId(models.ReviewRequestId) models.AnswerOptions

	Count() int
	Create(*models.AnswerOption) models.Errors
	OneById(models.AnswerOptionId) models.AnswerOption
	Update(models.AnswerOptionId, *models.AnswerOption) models.Errors
}

func NewAnswerOptionRepo() AnswerOptionRepo {
	return newAnswerOptionRepo(defaultSession.DB(""))
}

func newAnswerOptionRepo(db *mgo.Database) AnswerOptionRepo {
	collection := db.C("answer_options")
	return realAnswerOptionRepo{
		collection:   collection,
		questionRepo: newQuestionRepo(db),
	}
}

type realAnswerOptionRepo struct {
	collection   *mgo.Collection
	questionRepo QuestionRepo
}

func (r realAnswerOptionRepo) AllByReviewRequestId(id models.ReviewRequestId) models.AnswerOptions {
	questions := r.questionRepo.AllByReviewRequestId(id)

	ids := questions.AnswerOptionIds()

	answerOptions := models.AnswerOptions{}
	panicIf(r.collection.Find(m{"_id": m{"$in": ids}}).All(&answerOptions))
	return answerOptions
}

func (r realAnswerOptionRepo) Count() int { return count(r.collection) }

func (r realAnswerOptionRepo) Create(o *models.AnswerOption) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.AnswerOptionId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realAnswerOptionRepo) OneById(id models.AnswerOptionId) (o models.AnswerOption) {
	oneById(r.collection, id, &o)
	return
}

func (r realAnswerOptionRepo) Update(id models.AnswerOptionId, o *models.AnswerOption) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
