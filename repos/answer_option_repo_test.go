package repos

import (
	"testing"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAnswerOptionRepo(t *testing.T) {
	db := openSession("mongodb://localhost").DB(nextTestDbName())
	defer db.Session.Close()
	require.Nil(t, db.DropDatabase())

	repo := newAnswerOptionRepo(db)
	assert.Equal(t, 0, repo.Count())

	var (
		answerOptionId  = models.NewAnswerOptionId()
		questionId      = models.NewQuestionId()
		questionnaireId = models.NewQuestionnaireId()
		reviewRequestId = models.NewReviewRequestId()
	)

	{ // setup
		seedQuestion(t, db, &models.Question{
			Id:              questionId,
			AnswerOptionIds: models.AnswerOptionIds{answerOptionId},
		})

		seedQuestionnaire(t, db, &models.Questionnaire{
			Id:          questionnaireId,
			QuestionIds: models.QuestionIds{questionId},
		})

		seedReviewRequest(t, db, &models.ReviewRequest{
			Id:              reviewRequestId,
			QuestionnaireId: questionnaireId,
		})
	}

	{
		// create an answer option
		require.Equal(t,
			noErrors,
			repo.Create(&models.AnswerOption{
				Id: answerOptionId,
			}))
		assert.Equal(t, 1, repo.Count())
	}

	{ // look the answer option up by id..
		o := repo.OneById(answerOptionId)
		assert.Equal(t, answerOptionId, o.Id)
	}

	{ // look all answer options up by review request id..
		o := repo.AllByReviewRequestId(reviewRequestId)
		require.Equal(t, 1, len(o))
		assert.Equal(t, answerOptionId, o[0].Id)
	}

}
