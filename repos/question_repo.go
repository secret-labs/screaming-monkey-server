package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type QuestionRepo interface {
	AllByQuestionIds(models.QuestionIds) models.Questions
	AllByReviewRequestId(models.ReviewRequestId) models.Questions

	Count() int
	Create(*models.Question) models.Errors
	OneById(models.QuestionId) models.Question
	Update(models.QuestionId, *models.Question) models.Errors
}

func NewQuestionRepo() QuestionRepo {
	return newQuestionRepo(defaultSession.DB(""))
}

func newQuestionRepo(db *mgo.Database) QuestionRepo {
	collection := db.C("questions")
	return realQuestionRepo{
		collection:        collection,
		questionnaireRepo: newQuestionnaireRepo(db),
		reviewRequestRepo: newReviewRequestRepo(db),
	}
}

type realQuestionRepo struct {
	collection        *mgo.Collection
	questionnaireRepo QuestionnaireRepo
	reviewRequestRepo ReviewRequestRepo
}

func (r realQuestionRepo) AllByQuestionIds(ids models.QuestionIds) models.Questions {
	questions := models.Questions{}
	panicIf(r.collection.Find(m{"_id": m{"$in": ids}}).All(&questions))
	return questions
}

func (r realQuestionRepo) AllByReviewRequestId(id models.ReviewRequestId) models.Questions {
	reviewRequest := r.reviewRequestRepo.OneById(id)
	questionnaire := r.questionnaireRepo.OneById(reviewRequest.QuestionnaireId)

	return r.AllByQuestionIds(questionnaire.QuestionIds)
}

func (r realQuestionRepo) Count() int {
	return count(r.collection)
}

func (r realQuestionRepo) Create(o *models.Question) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.QuestionId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realQuestionRepo) OneById(id models.QuestionId) (o models.Question) {
	oneById(r.collection, id, &o)
	return
}

func (r realQuestionRepo) Update(id models.QuestionId, o *models.Question) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
