package repos

import (
	"testing"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestQuestionRepo(t *testing.T) {
	db := openSession("mongodb://localhost").DB(nextTestDbName())
	defer db.Session.Close()
	require.Nil(t, db.DropDatabase())

	repo := newQuestionRepo(db)
	assert.Equal(t, 0, repo.Count())

	var (
		questionId1     = models.NewQuestionId()
		questionId2     = models.NewQuestionId()
		questionnaireId = models.NewQuestionnaireId()
		reviewRequestId = models.NewReviewRequestId()
	)

	{ // setup
		seedQuestionnaire(t, db, &models.Questionnaire{
			Id: questionnaireId,
			QuestionIds: models.QuestionIds{
				questionId1,
				questionId2,
			},
		})

		seedReviewRequest(t, db, &models.ReviewRequest{
			Id:              reviewRequestId,
			QuestionnaireId: questionnaireId,
		})
	}

	seedQuestion(t, db, &models.Question{Id: questionId1})
	assert.Equal(t, 1, repo.Count())

	seedQuestion(t, db, &models.Question{Id: questionId2})
	assert.Equal(t, 2, repo.Count())

	{ // lookup one question by id..
		o := repo.OneById(questionId1)
		assert.Equal(t, questionId1, o.Id)
	}

	{ // lookup another question by id..
		o := repo.OneById(questionId2)
		assert.Equal(t, questionId2, o.Id)
	}

	{ // AllByQuestionIds w/ one id
		o := repo.AllByQuestionIds(models.QuestionIds{questionId1})
		require.Equal(t, 1, len(o))
		assert.Equal(t, questionId1, o[0].Id)
	}

	{ // AllByQuestionIds w/ a different id
		o := repo.AllByQuestionIds(models.QuestionIds{
			questionId2,
		})
		require.Equal(t, 1, len(o))
		assert.Equal(t, questionId2, o[0].Id)
	}

	{ // AllByQuestionIds w/ two different ids
		o := repo.AllByQuestionIds(models.QuestionIds{
			questionId1,
			questionId2,
		})
		require.Equal(t, 2, len(o))
		assert.Equal(t, questionId1, o[0].Id)
		assert.Equal(t, questionId2, o[1].Id)
	}

	{ // look all answer options up by review request id..
		o := repo.AllByReviewRequestId(reviewRequestId)
		require.Equal(t, 2, len(o))
		assert.Equal(t, questionId1, o[0].Id)
		assert.Equal(t, questionId2, o[1].Id)
	}
}
