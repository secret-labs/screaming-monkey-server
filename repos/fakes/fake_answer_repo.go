package fakes

import "bitbucket.org/secret-labs/screaming-monkey-server/models"

type FakeAnswerOptionRepo struct {
	FakeAllByReviewRequestId func(models.ReviewRequestId) models.AnswerOptions
	FakeCount                func() int
	FakeCreate               func(*models.AnswerOption) models.Errors
	FakeOneById              func(models.AnswerOptionId) models.AnswerOption
	FakeUpdate               func(models.AnswerOptionId, *models.AnswerOption) models.Errors
}

func (f FakeAnswerOptionRepo) AllByReviewRequestId(id models.ReviewRequestId) models.AnswerOptions {
	return f.FakeAllByReviewRequestId(id)
}

func (f FakeAnswerOptionRepo) Count() int {
	return f.Count()
}

func (f FakeAnswerOptionRepo) Create(o *models.AnswerOption) models.Errors {
	return f.FakeCreate(o)
}

func (f FakeAnswerOptionRepo) OneById(id models.AnswerOptionId) models.AnswerOption {
	return f.FakeOneById(id)
}

func (f FakeAnswerOptionRepo) Update(id models.AnswerOptionId, o *models.AnswerOption) models.Errors {
	return f.FakeUpdate(id, o)
}
