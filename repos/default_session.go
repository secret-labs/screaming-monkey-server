package repos

import (
	"sync"

	"bitbucket.org/secret-labs/screaming-monkey-server/env"

	"github.com/levicook/slog"
	"gopkg.in/mgo.v2"
)

var defaultSessionMutex sync.Mutex
var defaultSession *mgo.Session

func openSession(uri string) *mgo.Session {
	s, e := mgo.Dial(uri)
	slog.PanicIf(e)
	return s
}

func OpenDefaultSession() {
	defaultSessionMutex.Lock()
	defaultSession = openSession(env.MONGO_URI())
	defaultSessionMutex.Unlock()
}

func CloseDefaultSession() {
	defaultSessionMutex.Lock()
	if defaultSession != nil {
		defaultSession.Close()
	}
	defaultSessionMutex.Unlock()
}
