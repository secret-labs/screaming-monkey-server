package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type ReviewRequestRepo interface {
	AllByUserId(models.UserId) models.ReviewRequests

	Count() int
	Create(*models.ReviewRequest) models.Errors
	OneById(models.ReviewRequestId) models.ReviewRequest
	Update(models.ReviewRequestId, *models.ReviewRequest) models.Errors
}

func NewReviewRequestRepo() ReviewRequestRepo {
	return newReviewRequestRepo(defaultSession.DB(""))
}

func newReviewRequestRepo(db *mgo.Database) ReviewRequestRepo {
	collection := db.C("review_requests")
	return realReviewRequestRepo{collection}
}

type realReviewRequestRepo struct {
	collection *mgo.Collection
}

func (r realReviewRequestRepo) AllByUserId(id models.UserId) models.ReviewRequests {
	k := m{
		"$or": d{
			{"reviewerUserId", id},
			{"revieweeUserId", id},
		}}

	reviewRequests := models.ReviewRequests{}
	panicIf(r.collection.Find(k).All(&reviewRequests))
	return reviewRequests
}

func (r realReviewRequestRepo) Count() int {
	return count(r.collection)
}

func (r realReviewRequestRepo) Create(o *models.ReviewRequest) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.ReviewRequestId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realReviewRequestRepo) OneById(id models.ReviewRequestId) (o models.ReviewRequest) {
	oneById(r.collection, id, &o)
	return
}

func (r realReviewRequestRepo) Update(id models.ReviewRequestId, o *models.ReviewRequest) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
