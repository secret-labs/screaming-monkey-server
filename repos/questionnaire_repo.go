package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type QuestionnaireRepo interface {
	Count() int
	Create(*models.Questionnaire) models.Errors
	OneById(models.QuestionnaireId) models.Questionnaire
	Update(models.QuestionnaireId, *models.Questionnaire) models.Errors
}

func NewQuestionnaireRepo() QuestionnaireRepo {
	return newQuestionnaireRepo(defaultSession.DB(""))
}

func newQuestionnaireRepo(db *mgo.Database) QuestionnaireRepo {
	collection := db.C("questionnaires")
	return realQuestionnaireRepo{collection}
}

type realQuestionnaireRepo struct {
	collection *mgo.Collection
}

func (r realQuestionnaireRepo) Count() int { return count(r.collection) }

func (r realQuestionnaireRepo) Create(o *models.Questionnaire) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.QuestionnaireId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realQuestionnaireRepo) OneById(id models.QuestionnaireId) (o models.Questionnaire) {
	oneById(r.collection, id, &o)
	return
}

func (r realQuestionnaireRepo) Update(id models.QuestionnaireId, o *models.Questionnaire) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
