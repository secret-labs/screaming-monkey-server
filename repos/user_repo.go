package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type UserRepo interface {
	Count() int
	Create(*models.User) models.Errors
	OneById(models.UserId) models.User
	Update(models.UserId, *models.User) models.Errors
}

func NewUserRepo() UserRepo {
	return newUserRepo(defaultSession.DB(""))
}

func newUserRepo(db *mgo.Database) UserRepo {
	collection := db.C("users")
	return realUserRepo{collection}
}

type realUserRepo struct {
	collection *mgo.Collection
}

func (r realUserRepo) Count() int { return count(r.collection) }

func (r realUserRepo) Create(o *models.User) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.UserId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realUserRepo) OneById(id models.UserId) (o models.User) {
	oneById(r.collection, id, &o)
	return
}

func (r realUserRepo) Update(id models.UserId, o *models.User) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
