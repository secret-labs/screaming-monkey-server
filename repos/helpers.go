package repos

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"

	"github.com/levicook/slog"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type d bson.D
type m bson.M

type errorer interface {
	Errors() models.Errors
}

var (
	errorf  = fmt.Errorf
	sprintf = fmt.Sprintf
	panicIf = slog.PanicIf

	noErrors models.Errors
	zeroUser models.User
	zeroTime time.Time

	contains = strings.Contains
)

func count(c *mgo.Collection) int {
	i, err := c.Count()
	panicIf(err)
	return i
}

func newId() string {
	return bson.NewObjectId().Hex()
}

func now() time.Time {
	return time.Now().UTC().Round(time.Microsecond)
}

func oneById(c *mgo.Collection, id interface{}, one interface{}) {
	err := c.FindId(id).One(one)

	if err == mgo.ErrNotFound {
		err = nil
	}

	panicIf(err)
}

func put(c *mgo.Collection, id interface{}, o errorer) error {
	if e := o.Errors(); e.Present() {
		return e
	}

	doc := toDoc(o)

	return set(c, id, doc)
}

func set(c *mgo.Collection, id interface{}, doc m) error {
	delete(doc, "createdAt")
	return c.UpdateId(id, m{"$set": doc})
}

func toDoc(v interface{}) (doc m) {
	raw, err := bson.Marshal(v)
	panicIf(err)
	panicIf(bson.Unmarshal(raw, &doc))
	return
}

func (doc m) populate(v interface{}) {
	raw, err := bson.Marshal(doc)
	panicIf(err)
	panicIf(bson.Unmarshal(raw, v))
}
