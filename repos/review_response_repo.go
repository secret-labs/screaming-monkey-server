package repos

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"gopkg.in/mgo.v2"
)

type ReviewResponseRepo interface {
	Count() int
	Create(*models.ReviewResponse) models.Errors
	OneById(models.ReviewRequestId) models.ReviewResponse
	Update(models.ReviewRequestId, *models.ReviewResponse) models.Errors
}

func NewReviewResponseRepo() ReviewResponseRepo {
	return newReviewResponseRepo(defaultSession.DB(""))
}

func newReviewResponseRepo(db *mgo.Database) ReviewResponseRepo {
	collection := db.C("review_responses")
	return realReviewResponseRepo{collection}
}

type realReviewResponseRepo struct {
	collection *mgo.Collection
}

func (r realReviewResponseRepo) Count() int { return count(r.collection) }

func (r realReviewResponseRepo) Create(o *models.ReviewResponse) models.Errors {
	if e := o.Errors(); e.Present() {
		return e
	}

	if o.Id.Blank() {
		o.Id = models.ReviewRequestId(newId())
	}

	doc := toDoc(o)
	doc["createdAt"] = now()
	doc["updatedAt"] = now()

	err := r.collection.Insert(doc)

	panicIf(err)

	doc.populate(o)

	return noErrors
}

func (r realReviewResponseRepo) OneById(id models.ReviewRequestId) (o models.ReviewResponse) {
	oneById(r.collection, id, &o)
	return
}

func (r realReviewResponseRepo) Update(id models.ReviewRequestId, o *models.ReviewResponse) models.Errors {
	o.UpdatedAt = now()

	err := put(r.collection, id, o)

	if me, ok := err.(models.Errors); ok {
		return me
	}

	panicIf(err)

	return noErrors
}
