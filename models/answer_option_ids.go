package models

type AnswerOptionIds []AnswerOptionId

func (ls AnswerOptionIds) Intersect(rs AnswerOptionIds) AnswerOptionIds {
	intersection := AnswerOptionIds{}

	for _, l := range ls {
		for _, r := range rs {
			if l == r {
				intersection = append(intersection, l)
			}
		}
	}

	return intersection.Unique()
}

func (ls AnswerOptionIds) Union(rs AnswerOptionIds) AnswerOptionIds {
	return append(ls, rs...).Unique()
}

func (ids AnswerOptionIds) Unique() AnswerOptionIds {
	index := make(map[AnswerOptionId]struct{})

	for _, id := range ids {
		index[id] = struct{}{}
	}

	results := make(AnswerOptionIds, len(index))

	i := 0
	for id := range index {
		results[i] = id
		i++
	}

	return results
}

func (a AnswerOptionIds) Len() int           { return len(a) }
func (a AnswerOptionIds) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnswerOptionIds) Less(i, j int) bool { return a[i] < a[j] }
