package models

import (
	"encoding/json"

	"github.com/levicook/slog"
)

type CompositeErrors map[string]Errors

func (e CompositeErrors) Blank() bool   { return len(e) == 0 }
func (e CompositeErrors) Present() bool { return len(e) > 0 }

func (e CompositeErrors) Error() string {
	return e.String()
}

func (e CompositeErrors) String() string {
	data, err := json.Marshal(e)
	slog.PanicIf(err)
	return string(data)
}
