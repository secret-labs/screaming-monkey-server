package models

import (
	"encoding/json"

	"github.com/levicook/slog"
)

func NewErrors() Errors {
	return make(Errors)
}

type Errors map[string]string

func (e Errors) Blank() bool   { return len(e) == 0 }
func (e Errors) Present() bool { return len(e) > 0 }

func (e Errors) Error() string {
	return e.String()
}

func (e Errors) String() string {
	data, err := json.Marshal(e)
	slog.PanicIf(err)
	return string(data)
}
