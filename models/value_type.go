package models

import "bitbucket.org/secret-labs/screaming-monkey-server/models/value_type"

type ValueType string

func (qt ValueType) Valid() (valid bool) {
	switch qt {
	case
		value_type.Numeric,
		value_type.Boolean:
		valid = true
	}

	return
}
