package models

import "time"

type Question struct {
	Id              QuestionId      ` bson:"_id"             json:"id"              `
	Kind            QuestionType    ` bson:"kind"            json:"kind"            `
	Copy            string          ` bson:"copy"            json:"copy"            `
	AnswerOptionIds AnswerOptionIds ` bson:"answerOptionIds" json:"answerOptionIds" `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (o Question) Created() bool {
	return !o.CreatedAt.IsZero()
}

func (o Question) Errors() Errors {
	e := make(Errors)

	return e
}
