package models

import "time"

type ReviewResponse struct {
	Id      ReviewRequestId       ` bson:"_id"     json:"id"      `
	Answers map[QuestionId]string ` bson:"answers" json:"answers" `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (o ReviewResponse) Created() bool {
	return !o.CreatedAt.IsZero()
}

func (o ReviewResponse) Errors() Errors {
	e := make(Errors)

	return e
}
