package models

import "time"

type AnswerOption struct {
	Id        AnswerOptionId ` bson:"_id"       json:"id"        `
	Label     string         ` bson:"label"     json:"label"     `
	Value     string         ` bson:"value"     json:"value"     `
	ValueType ValueType      ` bson:"valueType" json:"valueType" `
	Help      string         ` bson:"help"      json:"help"      `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (o AnswerOption) Created() bool {
	return !o.CreatedAt.IsZero()
}

func (o AnswerOption) Errors() Errors {
	e := make(Errors)

	return e
}
