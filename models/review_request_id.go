package models

import "gopkg.in/mgo.v2/bson"

type ReviewRequestId string

func NewReviewRequestId() ReviewRequestId { return ReviewRequestId(newId()) }

func (id ReviewRequestId) Blank() bool   { return id == "" }
func (id ReviewRequestId) Present() bool { return id != "" }

func (id ReviewRequestId) Valid() bool   { return bson.IsObjectIdHex(string(id)) }
func (id ReviewRequestId) Invalid() bool { return !id.Valid() }

func (id ReviewRequestId) GetBSON() (v interface{}, e error) {
	if id.Valid() {
		v = bson.ObjectIdHex(string(id))
	}
	return
}

func (id *ReviewRequestId) SetBSON(raw bson.Raw) error {
	var oid bson.ObjectId
	err := raw.Unmarshal(&oid)
	*id = ReviewRequestId(oid.Hex())
	return err
}
