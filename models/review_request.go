package models

import "time"

type ReviewRequest struct {
	Id              ReviewRequestId ` bson:"_id"             json:"id"              `
	ReviewerUserId  UserId          ` bson:"reviewerUserId"  json:"reviewerUserId"  `
	RevieweeUserId  UserId          ` bson:"revieweeUserId"  json:"revieweeUserId"  `
	QuestionnaireId QuestionnaireId ` bson:"questionnaireId" json:"questionnaireId" `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (m ReviewRequest) Created() bool {
	return !m.CreatedAt.IsZero()
}

func (m ReviewRequest) Errors() Errors {
	e := make(Errors)

	return e
}
