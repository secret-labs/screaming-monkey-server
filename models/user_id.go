package models

import "gopkg.in/mgo.v2/bson"

type UserId string

func NewUserId() UserId { return UserId(newId()) }

func (id UserId) Blank() bool   { return id == "" }
func (id UserId) Present() bool { return id != "" }

func (id UserId) Valid() bool   { return bson.IsObjectIdHex(string(id)) }
func (id UserId) Invalid() bool { return !id.Valid() }

func (id UserId) GetBSON() (v interface{}, e error) {
	if id.Valid() {
		v = bson.ObjectIdHex(string(id))
	}
	return
}

func (id *UserId) SetBSON(raw bson.Raw) error {
	var oid bson.ObjectId
	err := raw.Unmarshal(&oid)
	*id = UserId(oid.Hex())
	return err
}
