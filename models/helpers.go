package models

import (
	"fmt"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	sprintf   = fmt.Sprintf
	toLower   = strings.ToLower
	trimSpace = strings.TrimSpace
)

func newId() string {
	return bson.NewObjectId().Hex()
}

func now() time.Time {
	return time.Now().UTC().Round(time.Microsecond)
}
