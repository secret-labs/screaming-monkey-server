package models

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_AnswerOptionIds_Intersect(t *testing.T) {
	var (
		idsA = AnswerOptionIds{"a"}
		idsB = AnswerOptionIds{"a", "b"}
		idsC = AnswerOptionIds{"b"}
		idsD = AnswerOptionIds{}
	)

	//------------
	assert.Equal(t, 1, len(idsA.Intersect(idsA)))
	assert.Equal(t, 1, len(idsA.Intersect(idsB)))
	assert.Equal(t, 0, len(idsA.Intersect(idsC)))
	assert.Equal(t, 0, len(idsA.Intersect(idsD)))

	//------------
	assert.Equal(t, 1, len(idsB.Intersect(idsA)))
	assert.Equal(t, 2, len(idsB.Intersect(idsB)))
	assert.Equal(t, 1, len(idsB.Intersect(idsC)))
	assert.Equal(t, 0, len(idsB.Intersect(idsD)))

	//------------
	assert.Equal(t, 0, len(idsC.Intersect(idsA)))
	assert.Equal(t, 1, len(idsC.Intersect(idsB)))
	assert.Equal(t, 1, len(idsC.Intersect(idsC)))
	assert.Equal(t, 0, len(idsC.Intersect(idsD)))

	//------------
	assert.Equal(t, 0, len(idsD.Intersect(idsA)))
	assert.Equal(t, 0, len(idsD.Intersect(idsB)))
	assert.Equal(t, 0, len(idsD.Intersect(idsC)))
	assert.Equal(t, 0, len(idsD.Intersect(idsD)))
}

func Test_AnswerOptionIds_Unique(t *testing.T) {

	{
		ids := AnswerOptionIds{}.Unique()
		require.Equal(t, 0, len(ids))
	}

	{
		ids := AnswerOptionIds{"a", "b", "c"}.Unique()
		require.Equal(t, 3, len(ids))

		sort.Sort(ids)
		assert.Equal(t, "a", ids[0])
		assert.Equal(t, "b", ids[1])
		assert.Equal(t, "c", ids[2])
	}

	{
		ids := AnswerOptionIds{"a", "a", "b", "b", "c", "c"}.Unique()
		require.Equal(t, 3, len(ids))

		sort.Sort(ids)
		assert.Equal(t, "a", ids[0])
		assert.Equal(t, "b", ids[1])
		assert.Equal(t, "c", ids[2])
	}
}
