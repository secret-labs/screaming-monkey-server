package models

import "time"

type Questionnaire struct {
	Id          QuestionnaireId ` bson:"_id"         json:"id"          `
	Name        string          ` bson:"name"        json:"name"        `
	QuestionIds QuestionIds     ` bson:"questionIds" json:"questionIds" `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (o Questionnaire) Created() bool {
	return !o.CreatedAt.IsZero()
}

func (o Questionnaire) Errors() Errors {
	e := make(Errors)

	return e
}
