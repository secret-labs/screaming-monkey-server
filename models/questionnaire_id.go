package models

import "gopkg.in/mgo.v2/bson"

type QuestionnaireId string

func NewQuestionnaireId() QuestionnaireId { return QuestionnaireId(newId()) }

func (id QuestionnaireId) Blank() bool   { return id == "" }
func (id QuestionnaireId) Present() bool { return id != "" }

func (id QuestionnaireId) Valid() bool   { return bson.IsObjectIdHex(string(id)) }
func (id QuestionnaireId) Invalid() bool { return !id.Valid() }

func (id QuestionnaireId) GetBSON() (v interface{}, e error) {
	if id.Valid() {
		v = bson.ObjectIdHex(string(id))
	}
	return
}

func (id *QuestionnaireId) SetBSON(raw bson.Raw) error {
	var oid bson.ObjectId
	err := raw.Unmarshal(&oid)
	*id = QuestionnaireId(oid.Hex())
	return err
}
