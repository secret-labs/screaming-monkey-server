package models

import "gopkg.in/mgo.v2/bson"

type AnswerOptionId string

func NewAnswerOptionId() AnswerOptionId { return AnswerOptionId(newId()) }

func (id AnswerOptionId) Blank() bool   { return id == "" }
func (id AnswerOptionId) Present() bool { return id != "" }

func (id AnswerOptionId) Valid() bool   { return bson.IsObjectIdHex(string(id)) }
func (id AnswerOptionId) Invalid() bool { return !id.Valid() }

func (id AnswerOptionId) GetBSON() (v interface{}, e error) {
	if id.Valid() {
		v = bson.ObjectIdHex(string(id))
	}
	return
}

func (id *AnswerOptionId) SetBSON(raw bson.Raw) error {
	var oid bson.ObjectId
	err := raw.Unmarshal(&oid)
	*id = AnswerOptionId(oid.Hex())
	return err
}
