package models

type Questions []Question

func (questions Questions) AnswerOptionIds() AnswerOptionIds {
	ids := AnswerOptionIds{}

	for _, question := range questions {
		ids = append(ids, question.AnswerOptionIds...)
	}

	return ids.Unique()
}
