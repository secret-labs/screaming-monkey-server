package models

import (
	"time"
)

type User struct {
	Id        UserId ` bson:"_id"       json:"id" `
	SuperUser bool   ` bson:"superUser" json:"-"  `

	CreatedAt time.Time ` bson:"createdAt" json:"createdAt" `
	UpdatedAt time.Time ` bson:"updatedAt" json:"updatedAt" `
}

func (m User) Created() bool {
	return !m.CreatedAt.IsZero()
}

func (m User) Errors() Errors {
	e := make(Errors)

	return e
}
