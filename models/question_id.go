package models

import "gopkg.in/mgo.v2/bson"

type QuestionId string

func NewQuestionId() QuestionId { return QuestionId(newId()) }

func (id QuestionId) Blank() bool   { return id == "" }
func (id QuestionId) Present() bool { return id != "" }

func (id QuestionId) Valid() bool   { return bson.IsObjectIdHex(string(id)) }
func (id QuestionId) Invalid() bool { return !id.Valid() }

func (id QuestionId) GetBSON() (v interface{}, e error) {
	if id.Valid() {
		v = bson.ObjectIdHex(string(id))
	}
	return
}

func (id *QuestionId) SetBSON(raw bson.Raw) error {
	var oid bson.ObjectId
	err := raw.Unmarshal(&oid)
	*id = QuestionId(oid.Hex())
	return err
}
