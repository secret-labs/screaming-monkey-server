package models

import "bitbucket.org/secret-labs/screaming-monkey-server/models/question_type"

type QuestionType string

func (qt QuestionType) Valid() (valid bool) {
	switch qt {
	case
		question_type.SingleChoice,
		question_type.Rating:
		valid = true
	}

	return
}
