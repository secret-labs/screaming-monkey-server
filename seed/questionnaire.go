package seed

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
)

func seedQuestionnaire(o models.Questionnaire) {
	repo := repos.NewQuestionnaireRepo()

	if tmp := repo.OneById(o.Id); tmp.Created() {
		handleErrors(repo.Update(o.Id, &o))
	} else {
		handleErrors(repo.Create(&o))
	}
}
