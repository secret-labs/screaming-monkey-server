package seed

import "bitbucket.org/secret-labs/screaming-monkey-server/models"

func seedQuestionnaires() {

	seedQuestionnaire(models.Questionnaire{
		Id:   "548dbb4587150de4e4000001",
		Name: "default", // this is temporary
		QuestionIds: models.QuestionIds{
			"548dacf287150dba21000001",
			"548dacf287150dba21000002",
			"548dacf287150dba21000003",
			"548dacf287150dba21000004",
			"548dacf287150dba21000005",
			"548dacf287150dba21000006",
			"548dacf287150dba21000007",
			"548dacf287150dba21000008",
			"548dacf287150dba21000009",
			"548dacf287150dba2100000a",
		},
	})
}
