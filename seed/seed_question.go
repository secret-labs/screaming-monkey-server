package seed

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"
)

func seedQuestion(o models.Question) {
	repo := repos.NewQuestionRepo()

	if tmp := repo.OneById(o.Id); tmp.Created() {
		handleErrors(repo.Update(o.Id, &o))
	} else {
		handleErrors(repo.Create(&o))
	}
}
