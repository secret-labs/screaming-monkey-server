package seed

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/models/question_type"
)

func seedQuestions() {

	ratingAnswerOptionIds := models.AnswerOptionIds{
		"548da0e387150d82f0000002", // unacceptable
		"548da0e387150d82f0000003", // ...
		"548da0e387150d82f0000004", // ...
		"548da0e387150d82f0000005", // ...
		"548da0e387150d82f0000006", // u/c
	}

	yesNoAnswerOptionIds := models.AnswerOptionIds{
		"548da0e387150d82f0000007", // yes
		"548da0e387150d82f0000008", // no
	}

	questions := models.Questions{
		{
			Id:              "548dacf287150dba21000001",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**1.** **Clinical Assessment:**  
Diagnostic skill; performance of practical/technical procedures`,
		}, {
			Id:              "548dacf287150dba21000002",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**2.** **Patient Management:**  
Management of complex clinical problems; appropriate use of resources`,
		}, {
			Id:              "548dacf287150dba21000003",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**3.** **Reliability:**  
Conscientious and reliable; available for advice and help when needed; time management`,
		}, {
			Id:              "548dacf287150dba21000004",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**4.** **Professional Development:**  
Commitment to improving quality of service; keeps up-to-date with knowledge and skills`,
		}, {
			Id:              "548dacf287150dba21000005",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**5.** **Teaching and Training:**  
Contributes to the education and supervision of students and junior colleagues`,
		}, {
			Id:              "548dacf287150dba21000006",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**6.** **Verbal Communication:**  
Spoken English; communication with colleagues, patients, families and carers`,
		}, {
			Id:              "548dacf287150dba21000007",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**7.** **Empathy and Respect:**  
Is polite, considerate and respectful to patients and colleagues of all levels; compassion and empathy towards patients and their relatives`,
		}, {
			Id:              "548dacf287150dba21000008",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**8.** **Team Player:**  
Values the skills and contributions of multi-disciplinary team members`,
		}, {
			Id:              "548dacf287150dba21000009",
			AnswerOptionIds: ratingAnswerOptionIds,
			Kind:            question_type.Rating,
			Copy: `**9.** **Leadership:**  
Takes the leadership role when circumstances require; delegates appropriately`,
		}, {
			Id:              "548dacf287150dba2100000a",
			AnswerOptionIds: yesNoAnswerOptionIds,
			Kind:            question_type.SingleChoice,
			Copy:            `**10.** Do you have any concerns about the Probity or Health (physical or mental) of this doctor?`,
		},
	}

	for _, question := range questions {
		seedQuestion(question)
	}
}
