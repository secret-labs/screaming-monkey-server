package seed

import (
	"os"

	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"github.com/levicook/slog"
)

func handleErrors(errs models.Errors) {
	if errs.Present() {
		slog.Dump(errs)
		os.Exit(1)
	}
}
