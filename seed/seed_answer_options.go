package seed

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/models"
	"bitbucket.org/secret-labs/screaming-monkey-server/models/value_type"
)

func seedAnswerOptions() {
	ratingAnswerOptions := models.AnswerOptions{{
		Id:        "548da0e387150d82f0000002",
		Label:     "unacceptable",
		Value:     "0",
		ValueType: value_type.Numeric,
		Help:      "I have concerns",
	}, {
		Id:        "548da0e387150d82f0000003",
		Label:     "below average",
		Value:     "1",
		ValueType: value_type.Numeric,
		Help:      "improvement needed",
	}, {
		Id:        "548da0e387150d82f0000004",
		Label:     "good",
		Value:     "2",
		ValueType: value_type.Numeric,
		Help:      "doing a good job",
	}, {
		Id:        "548da0e387150d82f0000005",
		Label:     "outstanding",
		Value:     "3",
		ValueType: value_type.Numeric,
		Help:      "excellent performance",
	}, {
		Id:        "548da0e387150d82f0000006",
		Label:     "u/c",
		Value:     "3",
		ValueType: value_type.Numeric,
		Help:      "unable to comment",
	}}

	yesNoAnswerOptions := models.AnswerOptions{{
		Id:        "548da0e387150d82f0000007",
		Label:     "Yes",
		Value:     "true",
		ValueType: value_type.Boolean,
	}, {
		Id:        "548da0e387150d82f0000008",
		Label:     "No",
		Value:     "3",
		ValueType: value_type.Boolean,
	}}

	for _, answerOption := range ratingAnswerOptions {
		seedAnswerOption(answerOption)
	}

	for _, answerOption := range yesNoAnswerOptions {
		seedAnswerOption(answerOption)
	}
}
