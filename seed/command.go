package seed

import (
	"bitbucket.org/secret-labs/screaming-monkey-server/repos"

	"github.com/levicook/slog"
	"github.com/robmerrell/comandante"
)

const cmdName = "seed"
const cmdHelp = ""

func Command() *comandante.Command {
	return comandante.NewCommand(cmdName, cmdHelp, func() error {
		slog.SetPrefix(cmdName)

		repos.OpenDefaultSession()
		defer repos.CloseDefaultSession()

		seedQuestionnaires()

		return nil
	})
}
